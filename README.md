# Python TeraChem Cloud Client (TCC)

This is the python client library for connecting to the Terachem Cloud API service.
Previously called PyTC, this was rebranded with the addition of the C++ version of the client.

For more in-depth documentation, check out https://mtzgrouptcc.readthedocs.io/en/latest/readme.html.

For distribution information, check out https://anaconda.org/mtzgroup/tcc.

For Martinez group users on Fire, the Python 2.7 beta release of the client is now available as a user module on fire: `tcc/1.5.1`


## Installation

Download the repo and install the python client in your local Anaconda environment:

```
conda install -c mtzgroup tcc
```

For developers looking to install in dev mode:

1. ``source activate <your environment>``
2. ``git clone git@bitbucket.org:mtzcloud/tcc-python.git``
3. ``cd tcc-python``
4. ``pip install -e .``


Example Usage
-------------

Running the TCC client is fairly straightforward: you set up a client to talk to the server,
specify your job options, submit geometries, and then check for results.

TeraChem Cloud now includes user authentication.
Before submitting any jobs, a user must be created for you in TeraChem Cloud.
You will be given a user id and an API key, both of which must be included during job submission.

In your shell, make sure to set ``TCCLOUD_USER`` and ``TCCLOUD_API_KEY`` in the environment for the example scripts
to work properly.

```
export TCCLOUD_USER=<username>
export TCCLOUD_API_KEY=<api key>
```

On Fire, the following script will run a single point on water:

```
import numpy as np
import os
import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Define your molecule, in this case a single water. Generally,
# we would read in atoms and geometry from an .xyz file
atoms = ['O', 'H', 'H']
geom = np.array([[0.00000,  0.00000, -0.06852],
               [0.00000, -0.79069,  0.54370],
               [0.00000,  0.79069,  0.54370]])


## Initialize client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')

## you can also ask the REST API for some documentation
TC.help()

## Set the job specification
job_options = {
  # TCC options
  'runtype':      'energy',
  'jobname':      'h2o test',
  'units':        'bohr',

  # TeraChem engine options
  'atoms':        atoms,
  'charge':       0,
  'spinmult':     1,
  'closed_shell': True,
  'restricted':   True,

  'method':       'pbe0',
  'basis':        '6-31g',
}

## submit the *ab initio* calculation to the Terachem API server and wait for the result
result = TC.compute(geom, job_options)
print result
```

There are several ways to use the client:

1. `submit()` and `get_results()` provide an asynchronous submission mechanism for single jobs
2. `compute()` uses `submit()` and wraps `get_results()` in a blocking poll function called `poll_for_results()`. Note that jobs are not guaranteed to have finished by poll exit - it is still the user's responsibility to check the job status before proceeding.
3. `compute_bulk()` provides a simple way to parallelize one set of `job_options` over multiple geometries, similar to `compute()`. 


The basic example from above is also in *examples/simple-example.py*.

An example of parallel jobs done through the blocking interface can be found in *examples/parallel-example.py*,
while the nonblocking interface is shown in *examples/async-example.py*.

An example showing advanced TeraChem option passing can be found in *examples/fomo-example.py*.

Several other examples show how to reuse information between jobs,
including orbitals (*examples/guess-reuse-example.py*) and CI vectors (*examples/ci-vec-overlap.py*).

## Contact

* Keiran Thompson  <keiran@stanford.edu>
* Stefan Seritan   <sseritan@stanford.edu>
