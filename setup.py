from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()

__title__ = "TeraChem Cloud Client"
__copyright__ = "Martinez Group, Stanford University, CA, USA, Planet Earth"
__version__ = "2.0.1"
__status__ = "beta"


setup(name="tcc",
      version=__version__,
      description="Python remote client for TeraChem Cloud server",
      long_description=readme(),
      packages=find_packages(exclude=['examples', 'tests']),
      test_suite="tcc",
      install_requires=['requests','numpy'],
      python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*, !=3.6.*, <4',
      url='https://bitbucket.org/mtzcloud/tcc',
      project_urls={
          'Source': 'https://bitbucket.org/mtzcloud/tcc',
          'Tracker': 'https://bitbucket.org/mtzcloud/tcc/issues',
          'Documentation': 'https://mtzgrouptcc.readthedocs.io/en/latest/readme.html'
      },
      classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Utilities',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8'
      ],
      )
