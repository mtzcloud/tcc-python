"""Utilites to extract and compare info from TeraChem output files

Originally taken from TCAutotest2, modified for single file
"""

import numpy as np

def extract_inline(reffile, regex, col, single=True):
    """Extract floats from a certain column from every line
    matching the given regex.

    Args:
        reffile (str): Reference filename
        regex (re.RegexObject): Compiled regex expression
        col (int): Column to extract from line split (0-indexed)
        single (bool): Whether to stop at the first match

    Returns:
        tuple: np.ndarray of data
    """
    with open(reffile, 'r') as fh:
        lines = fh.readlines()

    entries = []
    for l in lines:
        if regex.search(l) is not None:
            entries.append(float(l.split()[col]))

            if single is True:
                break

    return np.array(entries)

def extract_block(reffile, regex, start_line, num_lines, start_col, num_cols, single=True):
    """Extract array of floats in a block following a regex match.

    Args:
        reffile (str): Reference filename
        regex (re.RegexObject): Compiled regex expression
        start_line (int): Line to start block (0 starts at matched line)
        num_lines (int): Number of lines in block
        start_col (int): Column to start block (inclusive, 0-indexed)
        num_cols (int): Number of columns in block
        single (bool): Whether to stop at the first match

    Returns:
        tuple: np.ndarray of data
    """
    with open(reffile, 'r') as fh:
        lines = fh.readlines()

    entries = []
    for i, l in enumerate(lines):
        if regex.search(l) is not None:
            for bl in lines[(i+start_line):(i+start_line+num_lines)]:
                entries.append([float(entry) for entry in bl.split()[start_col:(start_col+num_cols)]])

            if single is True:
                break
    
    return np.array(entries)

def extract_geom(geomfile):
    """Extract geometry and TCC options from files

    Args:
        geomfile (str): XYZ geometry filename

    Returns:
        list: Geometry (in Angstrom)
        dict: TCC options based on inputfile
    """
    with open(geomfile, 'r') as fh:
        geomlines = fh.readlines()

    natom = int(geomlines[0])
    X = []
    atoms = []
    for l in geomlines[2:2+natom]:
        entries = l.split()
        atoms.append(entries[0])
        X.append([float(e) for e in entries[1:]])

    return np.array(X), atoms

def extract_input(inputfile):
    """Extract TCC options from file

    Args:
        inputfile (str): TeraChem input deck filename

    Returns:
        dict: TCC options based on inputfile (still needs atoms)
    """
    with open(inputfile, 'r') as fh:
        inputlines = fh.readlines()

    options = {}
    for l in inputlines:
        entries = l.split()
        if len(entries) and not entries[0].startswith('#'):
            options[entries[0]] = ' '.join(entries[1:])

    # Pre-process options for TCC
    options['runtype'] = options.get('run', 'energy')
    options['jobname'] = inputfile.replace('/', '_')
    options['units'] = 'angstrom'

    restricted = closed = True
    if options['method'].startswith('u'):
        restricted = closed = False
    elif options['method'].startswith('ro'):
        closed = False
    options['restricted'] = restricted
    options['closed_shell'] = closed

    options['basis'] = options['basis'].replace('*', 's').replace('+', 'p')

    # I can probably fake some type checking for now
    # In the future, this should be a utility method that gets type info directly from server
    int_keys = ['charge', 'spinmult', 'closed', 'active', 'cassinglets', 'castarget', 'nacstate1', 'nacstate2',
        'cistarget', 'cisnumstates']
    for ik in int_keys:
        if ik in options.keys():
            options[ik] = int(options[ik])

    float_keys = ['threall', 'rc_w', 'alpha', 'epsilon', 'pcm_radii_scale']
    for fk in float_keys:
        if fk in options.keys():
            options[fk] = float(options[fk])

    delete_keys = ['coordinates', 'scrdir', 'run']
    for dk in delete_keys:
        if dk in options.keys():
            del options[dk]

    return options
