"""Simple energy/gradient test
"""

import json
import numpy as np
import os
import re
import sys
import unittest
# Randomize order
#unittest.TestLoader.sortTestMethodsUsing = None

from tcc import Client

from utils import extract_geom, extract_input, extract_inline, extract_block

DEBYE_TO_AU = 0.393456 # a.u./Debye

URL = 'http://fire-20-06:30080'
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']
client = Client(
    url=URL,
    user=USER,
    api_key=API_KEY,
    engine='TeraChem')


class TestTeraChemEngine(unittest.TestCase):
    def test_h2o(self):
        X, atoms = extract_geom('terachem_reference/h2o+pcm/h2o.xyz')
        job_options = extract_input('terachem_reference/h2o+pcm/h2o.in')
        job_options['atoms'] = atoms
        job_options['units'] = "angstrom"

        results = client.compute(X, job_options)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/h2o+pcm/h2o.out', E_re, col=2)
        G_re = re.compile('Gradient units')
        ref_G = extract_block('terachem_reference/h2o+pcm/h2o.out', G_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()

        self.assertTrue(np.allclose(ref_E, results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_G, results['gradient'], 0.0, 1e-5))

    def test_h2o_pcm(self):
        X, atoms = extract_geom('terachem_reference/h2o+pcm/h2o.xyz')
        job_options = extract_input('terachem_reference/h2o+pcm/pcm.in')
        job_options['atoms'] = atoms
        job_options['units'] = "angstrom"

        results = client.compute(X, job_options)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/h2o+pcm/pcm.out', E_re, col=2)
        G_re = re.compile('Gradient units')
        ref_G = extract_block('terachem_reference/h2o+pcm/pcm.out', G_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()

        self.assertTrue(np.allclose(ref_E, results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_G, results['gradient'], 0.0, 1e-5))

    def test_h2os_blocking(self):
        X, atoms = extract_geom('terachem_reference/h2o+pcm/h2o.xyz')
        job_options = extract_input('terachem_reference/h2o+pcm/h2o.in')
        job_options['atoms'] = atoms
        job_options['units'] = "angstrom"
        
        results = client.compute_bulk([X for i in range(10)], job_options)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/h2o+pcm/h2o.out', E_re, col=2)

        self.assertTrue(np.allclose((ref_E,)*10, [r['energy'] for r in results], 0.0, 1e-5))

    def test_h2os_async(self):
        X, atoms = extract_geom('terachem_reference/h2o+pcm/h2o.xyz')
        job_options = extract_input('terachem_reference/h2o+pcm/h2o.in')
        job_options['atoms'] = atoms
        job_options['units'] = "angstrom"
        
        job_ids = [client.submit(X, job_options) for i in range(10)]

        results = client.poll_for_bulk_results(job_ids)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/h2o+pcm/h2o.out', E_re, col=2)

        self.assertTrue(np.allclose((ref_E,)*10, [r['energy'] for r in results], 0.0, 1e-5))

    def test_FOMO_CASCI_grad(self):
        X, atoms = extract_geom('terachem_reference/fomo-casci/ethene.xyz')

        grad_options = extract_input('terachem_reference/fomo-casci/grad.in')
        grad_options['atoms'] = atoms
        grad_options['units'] = "angstrom"

        grad_results = client.compute(X, grad_options)

        E_re = re.compile('Singlet state\s+\d energy')
        ref_Es = extract_inline('terachem_reference/fomo-casci/grad.out', E_re, col=4, single=False)
        G_re = re.compile('Gradient units')
        ref_G = extract_block('terachem_reference/fomo-casci/grad.out', G_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()

        self.assertTrue(np.allclose(ref_Es, grad_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_G, grad_results['gradient'], 0.0, 1e-5))

    def test_FOMO_CASCI_nac(self):
        X, atoms = extract_geom('terachem_reference/fomo-casci/ethene.xyz')

        nac_options = extract_input('terachem_reference/fomo-casci/nac.in')
        nac_options['atoms'] = atoms
        nac_options['units'] = "angstrom"

        nac_results = client.compute(X, nac_options)

        E_re = re.compile('Singlet state\s+\d energy')
        ref_Es = extract_inline('terachem_reference/fomo-casci/nac.out', E_re, col=4, single=False)
        NAC_re = re.compile('Gradient units')
        ref_NAC = extract_block('terachem_reference/fomo-casci/nac.out', NAC_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()

        self.assertTrue(np.allclose(ref_Es, nac_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_NAC, nac_results['nacme'], 0.0, 1e-2))

    def test_TDDFT_grad(self):
        X, atoms = extract_geom('terachem_reference/tddft/ethene.xyz')

        grad_options = extract_input('terachem_reference/tddft/grad.in')
        grad_options['atoms'] = atoms
        grad_options['units'] = "angstrom"

        grad_results = client.compute(X, grad_options)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/tddft/grad.out', E_re, col=2)
        ExE_re = re.compile('Total Energy')
        ref_ExEs = extract_block('terachem_reference/tddft/grad.out', ExE_re,
            start_line=2, num_lines=grad_options['cisnumstates'], start_col=1, num_cols=1)
        ref_Es = list(ref_E) + [ExE[0] for ExE in ref_ExEs]
        G_re = re.compile('Gradient units')
        ref_G = extract_block('terachem_reference/tddft/grad.out', G_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()
        UD_re = re.compile('Unrelaxed excited state dipole moments')
        ref_UDs = extract_block('terachem_reference/tddft/grad.out', UD_re,
            start_line=4, num_lines=grad_options['cisnumstates'], start_col=1, num_cols=3) * DEBYE_TO_AU
        RD_re = re.compile('Relaxed excited state dipole moments')
        ref_RDs = extract_block('terachem_reference/tddft/grad.out', RD_re,
            start_line=4, num_lines=grad_options['cisnumstates'], start_col=1, num_cols=3) * DEBYE_TO_AU
        g2e_TD_re = re.compile('Transition dipole moments:')
        g2e_TDs = extract_block('terachem_reference/tddft/grad.out', g2e_TD_re,
            start_line=4, num_lines=grad_options['cisnumstates'], start_col=1, num_cols=3)
        e2e_TD_re = re.compile('Transition dipole moments between excited states')
        e2e_TDs = extract_block('terachem_reference/tddft/grad.out', e2e_TD_re,
            start_line=4, num_lines=grad_options['cisnumstates'], start_col=3, num_cols=3)
        ref_TDs = np.vstack((g2e_TDs, e2e_TDs))

        self.assertTrue(np.allclose(ref_Es, grad_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_G, grad_results['gradient'], 0.0, 1e-4))
        self.assertTrue(np.allclose(ref_UDs, grad_results['cis_unrelaxed_dipoles'], 0.0, 1e-3))
        self.assertTrue(np.allclose(ref_RDs, grad_results['cis_relaxed_dipoles'], 0.0, 1e-3))
        self.assertTrue(np.allclose(np.abs(ref_TDs), np.abs(grad_results['cis_transition_dipoles']), 0.0, 1e-2))

    def test_TDDFT_nac(self):
        X, atoms = extract_geom('terachem_reference/tddft/ethene.xyz')

        nac_options = extract_input('terachem_reference/tddft/nac.in')
        nac_options['atoms'] = atoms
        nac_options['units'] = "angstrom"

        nac_results = client.compute(X, nac_options)

        E_re = re.compile('FINAL ENERGY')
        ref_E = extract_inline('terachem_reference/tddft/nac.out', E_re, col=2)
        ExE_re = re.compile('Total Energy')
        ref_ExEs = extract_block('terachem_reference/tddft/nac.out', ExE_re,
            start_line=2, num_lines=nac_options['cisnumstates'], start_col=1, num_cols=1)
        ref_Es = list(ref_E) + [ExE[0] for ExE in ref_ExEs]
        NAC_re = re.compile('Gradient units')
        ref_NAC = extract_block('terachem_reference/tddft/nac.out', NAC_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()
        UD_re = re.compile('Unrelaxed excited state dipole moments')
        ref_UDs = extract_block('terachem_reference/tddft/nac.out', UD_re,
            start_line=4, num_lines=nac_options['cisnumstates'], start_col=1, num_cols=3) * DEBYE_TO_AU
        RD_re = re.compile('Relaxed excited state dipole moments')
        ref_RDs = extract_block('terachem_reference/tddft/nac.out', RD_re,
            start_line=4, num_lines=nac_options['cisnumstates'], start_col=1, num_cols=3) * DEBYE_TO_AU
        g2e_TD_re = re.compile('Transition dipole moments:')
        g2e_TDs = extract_block('terachem_reference/tddft/nac.out', g2e_TD_re,
            start_line=4, num_lines=nac_options['cisnumstates'], start_col=1, num_cols=3)
        e2e_TD_re = re.compile('Transition dipole moments between excited states')
        e2e_TDs = extract_block('terachem_reference/tddft/nac.out', e2e_TD_re,
            start_line=4, num_lines=nac_options['cisnumstates'], start_col=3, num_cols=3)
        ref_TDs = np.vstack((g2e_TDs, e2e_TDs))

        self.assertTrue(np.allclose(ref_Es, nac_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(np.abs(ref_NAC), np.abs(nac_results['nacme']), 0.0, 1e-2))
        self.assertTrue(np.allclose(ref_UDs, nac_results['cis_unrelaxed_dipoles'], 0.0, 1e-3))
        self.assertTrue(np.allclose(ref_RDs, nac_results['cis_relaxed_dipoles'], 0.0, 1e-3))
        self.assertTrue(np.allclose(np.abs(ref_TDs), np.abs(nac_results['cis_transition_dipoles']), 0.0, 1e-2))

    def test_aCASSCF_grad(self):
        X, atoms = extract_geom('terachem_reference/casscf/ethene.xyz')

        grad_options = extract_input('terachem_reference/casscf/grad.in')
        grad_options['atoms'] = atoms
        grad_options['units'] = "angstrom"

        grad_results = client.compute(X, grad_options)

        E_re = re.compile('Singlet state\s+\d energy')
        ref_Es = extract_inline('terachem_reference/casscf/grad.out', E_re, col=4, single=False)
        G_re = re.compile('Gradient units')
        ref_G = extract_block('terachem_reference/casscf/grad.out', G_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()

        self.assertTrue(np.allclose(ref_Es, grad_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_G, grad_results['gradient'], 0.0, 1e-5))

    def test_aCASSCF_nac(self):
        X, atoms = extract_geom('terachem_reference/casscf/ethene.xyz')

        nac_options = extract_input('terachem_reference/casscf/nac.in')
        nac_options['atoms'] = atoms
        nac_options['units'] = "angstrom"

        nac_results = client.compute(X, nac_options)

        E_re = re.compile('Singlet state\s+\d energy')
        ref_Es = extract_inline('terachem_reference/casscf/nac.out', E_re, col=4, single=False)
        NAC_re = re.compile('Gradient units')
        ref_NAC = extract_block('terachem_reference/casscf/nac.out', NAC_re,
            start_line=3, num_lines=len(X), start_col=0, num_cols=3).flatten()
        TD_re = re.compile('Singlet state electronic transitions')
        ref_TD = extract_block('terachem_reference/casscf/nac.out', TD_re,
            start_line=6, num_lines=1, start_col=3, num_cols=3)

        self.assertTrue(np.allclose(ref_Es, nac_results['energy'], 0.0, 1e-5))
        self.assertTrue(np.allclose(ref_NAC, nac_results['nacme'], 0.0, 1e-2))
        self.assertTrue(np.allclose(np.abs(ref_TD), np.abs(nac_results['cas_transition_dipole']), 0.0, 1e-2))

    def test_ci_overlap(self):
        X1, atoms = extract_geom('terachem_reference/ci-overlap/ethene.xyz')
        X2, atoms = extract_geom('terachem_reference/ci-overlap/ethene2.xyz')

        # Initial CASCI jobs
        ci1_options = extract_input('terachem_reference/ci-overlap/ethene.in')
        ci1_options['atoms'] = atoms
        ci1_options['units'] = "angstrom"
        ci2_options = extract_input('terachem_reference/ci-overlap/ethene2.in')
        ci2_options['atoms'] = atoms
        ci2_options['units'] = "angstrom"

        ci1_results = client.compute(X1, ci1_options, max_poll=90)
        ci2_results = client.compute(X2, ci2_options, max_poll=90)

        # Overlap job
        overlap_options = extract_input('terachem_reference/ci-overlap/ci_overlap.in')
        overlap_options['atoms'] = atoms
        overlap_options['units'] = "angstrom"

        del overlap_options['old_coors']
        overlap_options['geom2'] = X2
        overlap_options['cvec1file'] = os.path.join(ci1_results['job_scr_dir'], 'CIvecs.Singlet.dat')
        overlap_options['cvec2file'] = os.path.join(ci2_results['job_scr_dir'], 'CIvecs.Singlet.dat')
        overlap_options['orb1afile'] = os.path.join(ci1_results['job_scr_dir'], 'c0')
        overlap_options['orb2afile'] = os.path.join(ci1_results['job_scr_dir'], 'c0')

        overlap_results = client.compute(X1, overlap_options)

        S_re = re.compile('Wavefunction overlap matrix')
        ref_overlap = extract_block('terachem_reference/ci-overlap/ci_overlap.out', S_re,
            start_line=1, num_lines=overlap_options['cassinglets'],
            start_col=0, num_cols=overlap_options['cassinglets']).flatten()

        # I think I didn't tune the precision down low enough for quantitative matching of the whole matrix
        self.assertTrue(np.allclose(np.abs(ref_overlap[0]), np.abs(overlap_results['ci_overlap'][0]), 0.0, 1e-2))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTeraChemEngine)
    results = unittest.TextTestRunner(verbosity=2).run(suite)

    if results.errors or results.failures:
        sys.exit(1)
