"""Tests for submitting valid but failing jobs
"""

import json
import numpy as np
import os
import sys
import unittest

from tcc import Client
from tcc.exceptions import TCCError, ServerError

URL = 'http://fire-20-06:30080'
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

base_options = {
    'runtype':      'gradient',
    'jobname':      'test',
    'units':        'bohr',

    'atoms':        ['O', 'H', 'H'],
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,
    'method':       'pbe0',
    'basis':        '6-31g',
}

h2o = np.array([[0.00000,  0.00000, -0.06852],
                [0.00000, -0.79069,  0.54370],
                [0.00000,  0.79069,  0.54370]])

client = Client(user=USER, api_key=API_KEY, url=URL, engine='terachem')

class TestFailure(unittest.TestCase):
    def test_copy(self):
        options = base_options.copy()
        options['guess'] = '/cloud-swap/file-does-not-exist'
        results = client.compute(h2o, options)
        self.assertTrue(results['job_status'] == 'FAILURE')

    def test_SCF_failure(self):
        options = base_options.copy()
        options['maxit'] = 2
        results = client.compute(h2o, options)
        self.assertTrue(results['job_status'] == 'FAILURE')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFailure)
    results = unittest.TextTestRunner(verbosity=2).run(suite)

    if results.errors or results.failures:
        sys.exit(1)
