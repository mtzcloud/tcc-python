"""Tests for messing up authentication and validation 
through the submit/get_results cycle
"""

import json
import numpy as np
import os
import sys
import unittest

from tcc import Client
from tcc.exceptions import TCCError, ServerError

URL = 'http://fire-20-06:30080'
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

base_options = {
    'runtype':      'gradient',
    'jobname':      'test',
    'units':        'bohr',

    'atoms':        ['O', 'H', 'H'],
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,
    'method':       'pbe0',
    'basis':        '6-31g',
}

h2o = np.array([[0.00000,  0.00000, -0.06852],
                [0.00000, -0.79069,  0.54370],
                [0.00000,  0.79069,  0.54370]])


class TestValidation(unittest.TestCase):
    def test_login(self):
        self.assertRaises(ServerError, Client, "", "", URL, "terachem")
        self.assertRaises(ServerError, Client, USER, "", URL, "terachem")

    def test_submit(self):
        client = Client(user=USER, api_key=API_KEY, url=URL, engine='terachem')

        # Mess up user/api_key and try to submit
        client.user = ""
        self.assertRaises(ServerError, client.submit, h2o, base_options)

        client.user = USER
        client.api_key = ""
        self.assertRaises(ServerError, client.submit, h2o, base_options)

        # Drop required option (one for TCC and one for TeraChem)
        client.api_key = API_KEY
        options = base_options.copy()
        del options['runtype']
        self.assertRaises(ServerError, client.submit, h2o, options)

        options = base_options.copy()
        del options['method']
        self.assertRaises(ServerError, client.submit, h2o, options)

        # Submit geometry that doesn't match job configuration
        self.assertRaises(ServerError, client.submit, [], options)
        
    def test_get_results(self):
        client = Client(user=USER, api_key=API_KEY, url=URL, engine='terachem')

        job_id = client.submit(h2o, base_options)

        # Mess up user/api_key and try to pull results
        client.user = ""
        self.assertRaises(ServerError, client.get_results, job_id)

        client.user = USER
        client.api_key = ""
        self.assertRaises(ServerError, client.get_results, job_id)

        # Give dummy job id
        client.api_key = API_KEY
        self.assertRaises(ServerError, client.get_results, "")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestValidation)
    results = unittest.TextTestRunner(verbosity=2).run(suite)

    if results.errors or results.failures:
        sys.exit(1)
