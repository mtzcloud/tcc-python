"""
Simple example of managing your own job_ids and polling the API for results.
In this example, you could continue to perform other calculations while waiting
for the server to run. Note also that your only reference to jobs on the server
is the job_id, hence the need for copy.copy()
"""

from collections import OrderedDict
import numpy as np
import os
import tcc
import time

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Define a collection of molecules, in this case waters
atoms = ['O', 'H', 'H']
ref_geom = np.array(
    [[0.00000, 0.00000, -0.06852],
     [0.00000, -0.79069, 0.54370],
     [0.00000, 0.79069, 0.54370]])

# jitter the reference water geometry to get 10 new geometries
geoms = [ref_geom + (np.random.random(ref_geom.shape)-0.5) for i in range(10)]


# Initialise client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')
#TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem')

## Set the job specification
tcc_options = {
    # TCC options
    'runtype':      'energy',
    'jobname':      'h2o test',
    'units':        'angstrom',

    # TeraChem engine options
    'atoms':        atoms,
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,

    'method':       'pbe0',
    'basis':        '6-31g',
}

# submit all the _ab initio_ calculations to the Terachem API server
job_ids = [TC.submit(geom, tcc_options) for geom in geoms]

# NOTE: Can do other work while letting jobs complete...

results_list = TC.poll_for_bulk_results(job_ids)
for results in results_list:
    print(results)
