"""Basic FOMO-CASCI example, computing the energy, gradient, and coupling
"""

import json
import numpy as np
import os
import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Ethene system
atoms = ['C', 'C', 'H', 'H', 'H', 'H']
geom = np.array([[ 0.35673483, -0.05087227, -0.47786734],
                 [ 1.61445821, -0.06684947, -0.02916681],
                 [-0.14997206,  0.87780529, -0.62680155],
                 [-0.16786485, -0.95561368, -0.69426370],
                 [ 2.15270896,  0.84221076,  0.19314809],
                 [ 2.16553127, -0.97886933,  0.15232587]])


# Initialise client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')
#TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem')

## Set the job specification
tcc_options = {
    # TCC options
    'runtype':      'energy',
    'jobname':      'ethene test',
    'units':        'angstrom',

    # TeraChem engine options
    'atoms':        atoms,
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,

    'method':       'pbe0',
    'basis':        '6-31g',

    # FOMO and CAS options
    'casci':        'yes',
    'fon':          'yes',
    'closed':       7,
    'active':       2,
    'cassinglets':  3,
}

# Compute ground and excited state energies
results = TC.compute(geom, tcc_options)
print("Energy job:")
print((json.dumps(results, indent=2, sort_keys=True)))

# TC options to specify target state for job_type='gradient'
# State is 0-based
grad_options = tcc_options.copy()
grad_options['runtype'] = 'gradient'
grad_options['castarget'] = 1
grad_options['castargetmult'] = 1

results = TC.compute(geom, grad_options)
print("Gradient job:")
print((json.dumps(results, indent=2, sort_keys=True)))

# TC options to specify target state for job_type='coupling'
# State is 0-based
nac_options = tcc_options.copy()
nac_options['runtype'] = 'coupling'
nac_options['nacstate1'] = 1
nac_options['nacstate2'] = 2
nac_options['castargetmult'] = 1

# compute job and collect results
results = TC.compute(geom, nac_options)
print("Coupling job:")
print((json.dumps(results, indent=2, sort_keys=True)))
