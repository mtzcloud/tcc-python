"""Basic PyTC client example including authentication
"""

import json
import numpy as np
import os
import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Define your molecule, in this case a single water. Generally,
# we would read in atoms and geometry from an .xyz file
atoms = ['O', 'H', 'H']
geom = np.array([[0.00000,  0.00000, -0.06852],
                 [0.00000, -0.79069,  0.54370],
                 [0.00000,  0.79069,  0.54370]])


# Initialize client
#TC = tcc.Client(url='http://localhost:30080', user=USER, api_key=API_KEY, engine='terachem', verbose=True)
TC = tcc.Client(url='http://fire-6-0-ext.slac.stanford.edu:80', user=USER, api_key=API_KEY, engine='terachem', verbose=True)
# TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem', verbose=True)
# TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem', verbose=True)

# Set the job specification
tcc_options = {
    # TCC options
    'runtype': 'energy',
    'jobname': 'h2o test',
    'units': 'angstrom',

    # TeraChem engine options
    'atoms': atoms,
    'charge': 0,
    'spinmult': 1,
    'closed_shell': True,
    'restricted': True,

    'method': 'pbe0',
    'basis': '6-31g',
}

# you can also ask the REST API for some documentation
#TC.help()

# submit the _ab initio_ calculation to the Terachem API server and wait for the result
result = TC.compute(geom, tcc_options)
#print(result)

# you can also do a nicer print using json
print((json.dumps(result, indent=2, sort_keys=True)))
