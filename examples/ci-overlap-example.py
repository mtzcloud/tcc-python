"""Basic CI overlap example using 
"""

import json
import os
import numpy as np
import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Ethene system
atoms = ['C', 'C', 'H', 'H', 'H', 'H']
geom = np.array([[ 0.35673483, -0.05087227, -0.47786734],
                 [ 1.61445821, -0.06684947, -0.02916681],
                 [-0.14997206,  0.87780529, -0.62680155],
                 [-0.16786485, -0.95561368, -0.69426370],
                 [ 2.15270896,  0.84221076,  0.19314809],
                 [ 2.16553127, -0.97886933,  0.15232587]])


# Initialise client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')
#TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem')

## Set the job specification
base_options = {
    # TCC options
    'runtype':      'energy',
    'jobname':      'ethene test',
    'units':        'angstrom',

    # TeraChem engine options
    'atoms':        atoms,
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,

    'method':       'pbe0',
    'basis':        '6-31g',

    'precision':    'double',
    'threall':      1e-20,

    'casci':        'yes',
    'closed':       5,
    'active':       6,
    'cassinglets':  10,
}

# Run CASCI to get CI vectors
casci_options = {
    'ci_driver':    'direct',
    'caswritevecs': 'yes',
}
options = dict(base_options, **casci_options) # Python trick to concatenate two dicts
results = TC.compute(geom, options)
print(results)


# Set up CI vector overlap
# Like the 'guess' keyword, this requires knowing what the dump files from previous job are called
# In this case, overlap with itself should be the identity
scr_dir = results['job_scr_dir']
overlap_options = {
    'geom2':        geom,
    'cvec1file':    os.path.join(scr_dir, 'CIvecs.Singlet.dat'),
    'cvec2file':    os.path.join(scr_dir, 'CIvecs.Singlet.dat'),
    'orb1afile':    os.path.join(scr_dir, 'c0'),
    'orb2afile':    os.path.join(scr_dir, 'c0'),
}
options = dict(base_options, **overlap_options)
options['runtype'] = 'ci_vec_overlap'
results = TC.compute(geom, options)
print(results)

print(('CI Vector Overlap:\n{}'.format(results['ci_overlap'])))
