"""Basic blocking example for multiple jobs
"""

import json
from functools import partial
from multiprocessing import Pool
import numpy as np
import os

import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Define a collection of molecules, in this case waters
atoms = ['O', 'H', 'H']
ref_geom = np.array(
    [[0.00000, 0.00000, -0.06852],
     [0.00000, -0.79069, 0.54370],
     [0.00000, 0.79069, 0.54370]])

# jitter the reference water geometry to get 10 new geometries
geoms = [ref_geom + (np.random.random(ref_geom.shape)-0.5) for i in range(10)]


# Initialise client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')
#TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem')

## Set the job specification
tcc_options = {
    # TCC options
    'runtype':      'energy',
    'jobname':      'h2o test',
    'units':        'angstrom',

    # TeraChem engine options
    'atoms':        atoms,
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,

    'method':       'pbe0',
    'basis':        '6-31g',
}

# submit all the _ab initio_ calculations to the Terachem API server and wait for the result
results = TC.compute_bulk(geoms, tcc_options)
print(('\n'.join([json.dumps(r) for r in results])))
