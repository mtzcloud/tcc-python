"""Basic orbital guess reuse example
"""

import json
import os
import numpy as np
import tcc

# Get authentication from the environment
USER = os.environ['TCCLOUD_USER']
API_KEY = os.environ['TCCLOUD_API_KEY']

# Ethene system
atoms = ['C', 'C', 'H', 'H', 'H', 'H']
geom = np.array([[ 0.35673483, -0.05087227, -0.47786734],
                 [ 1.61445821, -0.06684947, -0.02916681],
                 [-0.14997206,  0.87780529, -0.62680155],
                 [-0.16786485, -0.95561368, -0.69426370],
                 [ 2.15270896,  0.84221076,  0.19314809],
                 [ 2.16553127, -0.97886933,  0.15232587]])

# Initialise client
TC = tcc.Client(url='http://fire-05-31:30080', user=USER, api_key=API_KEY, engine='terachem')
#TC = tcc.Client(url='http://xs7-0001:30080', user=USER, api_key=API_KEY, engine='terachem')

## Set the job specification
tcc_options = {
    # TCC options
    'runtype':      'energy',
    'jobname':      'ethene test',
    'units':        'angstrom',

    # TeraChem engine options
    'atoms':        atoms,
    'charge':       0,
    'spinmult':     1,
    'closed_shell': True,
    'restricted':   True,

    'method':       'hf',
    'basis':        '6-31g',
}

# Run job to get orbitals
results = TC.compute(geom, tcc_options)
print(results)


# Run job reusing guess orbitals
guess_options = tcc_options.copy()
guess_options['guess'] = results['orbfile']
results = TC.compute(geom, guess_options)
print(results)

# Reset for unrestricted
tcc_options['closed_shell'] = False
tcc_options['restricted'] = False

results = TC.compute(geom, tcc_options)
print(results)

# Slightly different for unrestricted since we need alpha and beta
guess_options = tcc_options.copy()
guess_options['guess'] = '{} {}'.format(results['orbfile_a'], results['orbfile_b'])
results = TC.compute(geom, guess_options)
print(results)
